output "vpc_id" {
  value = aws_vpc.my_vpc.id
}

output "sg_id" {
  value = aws_security_group.sg.id
}

output "sn_id" {
  value = aws_subnet.public_sn.id
}