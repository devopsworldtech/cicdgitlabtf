# 1 vpc, 1 subnet, 1 sg

resource "aws_vpc" "my_vpc" {
  cidr_block = "10.10.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "myvpc"
  }
}

resource "aws_subnet" "public_sn" {
  vpc_id = aws_vpc.my_vpc.id
  cidr_block = "10.10.1.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "pb_sn1"
  }
}

resource "aws_security_group" "sg" {
  vpc_id = aws_vpc.my_vpc.id
  name = "my_sg"
  description = "Public Security"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}
