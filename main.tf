module "vpc" {
  source = "./vpc"
}

module "ec2" {
  source = "./web"
  sg_id = module.vpc.sg_id
  sn_id = module.vpc.sn_id
}