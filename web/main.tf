# EC2 Instance
resource "aws_instance" "server" {
  ami = "ami-0c101f26f147fa7fd"
  instance_type = "t2.micro"
  subnet_id = var.sn_id
  security_groups = [var.sg_id]

  tags = {
    Name = "myserver"
  }
}