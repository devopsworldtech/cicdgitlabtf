terraform {
  backend "s3" {
    bucket = "gitlabterraformcicd"
    key    = "state"
    region = "us-east-1"
  }
}
